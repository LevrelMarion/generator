import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class Groupe {
    List<Person> person=new ArrayList<>();

    public Groupe() {

    }

    public void addPerson(Person addName) {
        person.add(addName);
    }

    public String getList() {
        String personList = "";
        for (int i = 0; i < person.size(); ++i) {
            if(i<(person.size()-1))
            personList +=(person.get(i)).getName()+lineSeparator();
            else
            {personList +=(person.get(i)).getName();}
        }
        return personList;

    }
}
