import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class GroupeManagerTest {
    @Test
    void shouldGiveListGroup() {
        List<Person> personne = new ArrayList<>();
        personne.add(new Person("Norbert"));
        personne.add(new Person("Marion"));
        personne.add(new Person("Erwan"));
        personne.add(new Person("Jaquemine"));

        List<Person> groupe1 = new ArrayList<>();
        groupe1.add(new Person("Norbert"));
        groupe1.add(new Person("Marion"));

        List<Person> groupe2 = new ArrayList<>();
        groupe2.add(new Person("Norbert"));
        groupe2.add(new Person("Marion"));

        GroupeManager groupeManager = new GroupeManager(personne, 2);

        List<Groupe> groupes = groupeManager.algo();

        Assertions.assertEquals(2, groupes.size());
        Assertions.assertEquals(2,groupe1.size());
        Assertions.assertEquals(2,groupe2.size());


    }
}
