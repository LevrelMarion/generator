import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class GroupeTest {

    @Test
        // test tableau vide et tableau rempli

    void shouldGiveListPerson() {

        Person norbert=new Person("Norbert");
        Person erwan=new Person("Erwan");
        Person marion=new Person("Marion");
        Groupe groupePerson = new Groupe();

        groupePerson.addPerson(norbert);
        groupePerson.addPerson(erwan);
        groupePerson.addPerson(marion);
        Assertions.assertEquals("Norbert" + lineSeparator() + "Erwan" + lineSeparator() + "Marion", groupePerson.getList());
    }
    @Test
    void ShouldHaveNullList(){
        Groupe groupePerson=new Groupe();
        Assertions.assertEquals("",groupePerson.getList());
    }

}
